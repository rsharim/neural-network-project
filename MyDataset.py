from torch.utils.data import TensorDataset, DataLoader, Dataset
from PIL import Image

class MyDataset(Dataset):
    def __init__(self, df_data,transform=None):
        super().__init__()
        self.df = df_data.values
        
        self.transform = transform

    def __len__(self):
        return len(self.df)
    
    def __getitem__(self, index):
        img_path,label = self.df[index]
        image = Image.open(img_path)
        image = image.resize((50,50))

        if self.transform is not None:
            image = self.transform(image)
        return image, label