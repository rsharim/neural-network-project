import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

import torch 
import torch.nn as nn

import torchvision
import torchvision.transforms as transforms
import torchvision.models as models
from torch.utils.data import TensorDataset, DataLoader, Dataset

from MyDataset import MyDataset

#Find the patches
from glob import glob
imagePatches = glob('./data/IDC_regular_ps50_idx5/**/*.png', recursive=True)

#Set classes
images_df = pd.DataFrame()
images_df["images"] = imagePatches
# split the images to classes
images_df["labels"] = images_df['images'].str[-5]
images_df["labels"] = images_df["labels"].astype(int)

#batch_size = 128
# need max 64 to train vgg
batch_size = 64

train_set, test_set = train_test_split(images_df, stratify=images_df.labels, test_size=0.2)

# enhance training set using flips and rotations
train_transformers = transforms.Compose([
                                  transforms.Pad(64, padding_mode='reflect'),
                                  transforms.RandomHorizontalFlip(), 
                                  transforms.RandomVerticalFlip(),
                                  transforms.RandomRotation(20), 
                                  transforms.ToTensor(),
                                  transforms.Normalize(mean=[0.5, 0.5, 0.5],std=[0.5, 0.5, 0.5])])

test_transformers = transforms.Compose([
                                  transforms.Pad(64, padding_mode='reflect'),
                                  transforms.ToTensor(),
                                  transforms.Normalize(mean=[0.5, 0.5, 0.5],std=[0.5, 0.5, 0.5])])

train_dataset = MyDataset(df_data=train_set, transform=train_transformers)
test_dataset = MyDataset(df_data=test_set,transform=test_transformers)

train_loader = DataLoader(dataset = train_dataset, batch_size=batch_size, shuffle=True, num_workers=0)
test_loader = DataLoader(dataset = test_dataset, batch_size=batch_size//2, shuffle=False, num_workers=0)

n_epochs = 10
n_classes = 2
lr = 0.002

# if torch.cuda.is_available():
#     print('cuda avaliable')

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def log(line):
    print(line)
    f=open("out.txt", "a+")
    f.write(line + '\n')
    f.close()

def train(model):
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adamax(model.parameters(), lr=lr)
    total_step = len(train_loader)
    for epoch in range(n_epochs):
        model.train()
        for i, (images, labels) in enumerate(train_loader):
            images = images.to(device)
            labels = labels.to(device)
            
            # Forward pass
            outputs = model(images)
            loss = criterion(outputs, labels)
            
            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
            if (i+1) % 100 == 0:
                print ('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}' 
                    .format(epoch+1, n_epochs, i+1, total_step, loss.item()))

        print('Test:')
        model.eval()
        confusion_matrix = torch.zeros(2, 2)
        with torch.no_grad():
            correct = 0
            total = 0
            for images, labels in test_loader:
                images = images.to(device)
                labels = labels.to(device)
                outputs = model(images)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
                for t, p in zip(labels.view(-1), predicted.view(-1)):
                        confusion_matrix[t.long(), p.long()] += 1

            log('Test Accuracy of the model on the test images: {} %'.format(100 * correct / total))
            log('Confusion matrix: TN: {} FP: {} FN: {} TP: {}'.format(confusion_matrix[0 ,0], confusion_matrix[0 ,1], confusion_matrix[1 ,0], confusion_matrix[1 ,1]))
        # Save the model checkpoint
#         torch.save(model.state_dict(), 'model.ckpt')

from MyCnn import MyCNN

# Test num of filters
def train_filter(n_filters):
    log('****** n_filters = {} ********'.format(n_filters))
    model = MyCNN(linear_size = 4232 * n_filters, cl_filters = n_filters, cl_kernel = 2, cl_stride = 1).to(device)
    train(model)

def train_kernel_size(kernel_size, linear_size):
    log('****** kernel_size = {} ********'.format(kernel_size))
    model = MyCNN(linear_size = linear_size, cl_filters = 32, cl_kernel = kernel_size).to(device)
    train(model)


def train_stride(n_stride, linear_size):
    log('****** n_steps = {} ********'.format(n_stride))
    model = MyCNN(linear_size = linear_size, cl_filters = 32, cl_stride = n_stride, cl_kernel = n_stride).to(device)
    train(model)

def train_mp_kernel_stride(kernel_size, n_stride, linear_size):
    log('****** max pool size = {} {} ********'.format(kernel_size, n_stride))
    model = MyCNN(linear_size = linear_size, cl_filters = 32, cl_kernel = 3 , cl_stride = 3, mp_kernel = kernel_size, mp_stride = n_stride).to(device)
    train(model)

def train_ll(ll_depth, linear_size):
    log('****** linear layers = {} ********'.format(ll_depth))
    model = MyCNN(linear_size = linear_size, cl_filters = 32, cl_kernel = 3 , cl_stride = 3, mp_kernel = 3, mp_stride = 3, ll_depth = ll_depth).to(device)
    train(model)

def train_cl(linear_size):
    log('****** 4 conv layers ********')
    model = MyCNN(linear_size = linear_size, cl_filters = 32, cl_kernel = 3 , cl_stride = 3, mp_kernel = 3, mp_stride = 3).to(device)
    train(model)


# train_filter(8)
# train_filter(16)
# train_filter(32)
# train_filter(64)
# train_filter(128)

# train_kernel_size(1, 141376)
# train_kernel_size(3, 135424)
# train_kernel_size(4, 129600)
# train_kernel_size(5, 123904)

# train_stride(2, 9216)
# train_stride(3, 1600)
# train_stride(4, 576)
# train_stride(5, 256)

# train_mp_kernel_stride(3, 3, 128)
# train_mp_kernel_stride(3, 2, 1600)

# train_ll(2, 256)
# train_ll(3, 256)

# train_cl(256)

# def train_vgg():
#     vgg16 = models.vgg16_bn(pretrained=True)
#     # Freeze training for all layers
#     for param in vgg16.features.parameters():
#         param.require_grad = False
#     num_features = vgg16.classifier[6].in_features
#     features = list(vgg16.classifier.children())[:-1] # Remove last layer
#     features.extend([nn.Linear(num_features, 2)]) # Add our layer with 2 outputs
#     vgg16.classifier = nn.Sequential(*features) # Replace the model classifier
#     vgg16 = vgg16.to(device)
#     log('****** vgg layers ********')
#     train(vgg16)

# train_vgg()

def visualize_filters():
    model = MyCNN(linear_size = 256, cl_filters = 32, cl_kernel = 3 , cl_stride = 3, mp_kernel = 3, mp_stride = 3)
    torch.load('model.ckpt')
    conv1 = model.conv1
    weights = conv1.weight.data.numpy()
    # bias = conv1.bias.data.numpy()
    # normalize filter values to 0-1 so we can visualize them
    f_min, f_max = weights.min(), weights.max()
    filters = (weights - f_min) / (f_max - f_min)

    n_filters, ix = 4, 1

    from matplotlib import pyplot

    for l in range(8):
        start = l * 4
        stop = start + 4
        ix = 1
        for i in range(n_filters):
            # get the filter
            f = filters[i + start]
            # plot each channel separately
            for j in range(3):
                # specify subplot and turn of axis
                ax = pyplot.subplot(n_filters, 3, ix)
                ax.set_xticks([])
                ax.set_yticks([])
                # plot filter channel in grayscale
                if j == 0:
                    cmap = 'Reds'
                elif j == 1:
                    cmap = 'Greens'
                else:
                    cmap = 'Blues'
                pyplot.imshow(f[:, :, j], cmap=cmap)
                ix += 1
        # show the figure
        pyplot.savefig('filters_rgb_{}_{}.png'.format(start, stop))

visualize_filters()