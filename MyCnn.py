
import torch 
import torch.nn as nn
import torch.nn.functional as F

class MyCNN(nn.Module):
    def __init__(self, linear_size = 256, cl_filters = 3, cl_kernel = 3, cl_stride = 3, mp_kernel = 3, mp_stride = 3, ll_depth = 1):
        # ancestor constructor call
        super(MyCNN, self).__init__()
        self.ll_depth = ll_depth
        self.conv1 = nn.Conv2d(in_channels=3, out_channels = cl_filters, kernel_size = cl_kernel, padding=2, stride = cl_stride)
        self.conv2 = nn.Conv2d(in_channels=cl_filters, out_channels= cl_filters * 2, kernel_size= cl_kernel, padding=2 , stride = cl_stride)
        self.conv3 = nn.Conv2d(in_channels=cl_filters * 2, out_channels= cl_filters * 4, kernel_size= 2, padding=2 , stride = 1)
        self.conv4 = nn.Conv2d(in_channels=cl_filters * 4, out_channels= cl_filters * 8, kernel_size= 2, padding=2 , stride = 1)
     
        self.bn1 = nn.BatchNorm2d(cl_filters)
        self.bn2 = nn.BatchNorm2d(cl_filters * 2)
        self.bn3 = nn.BatchNorm2d(cl_filters * 4)
        self.bn4 = nn.BatchNorm2d(cl_filters * 8)
     
        self.pool = nn.MaxPool2d(kernel_size = mp_kernel, stride = mp_stride)
     
        if ll_depth == 1:
            self.output_layer = nn.Linear(linear_size, 2)
        elif ll_depth == 2:
            self.linear_layer = nn.Linear(linear_size, int(linear_size / 2))
            self.output_layer = nn.Linear(int(linear_size / 2), 2)
        elif ll_depth == 3:
            self.linear_layer = nn.Linear(linear_size, int(linear_size / 2))
            self.linear_layer2 = nn.Linear(int(linear_size / 2), int(linear_size / 4))
            self.output_layer = nn.Linear(int(linear_size / 4), 2)

    
    def forward(self, x):
        x = self.pool(F.relu(self.bn1(self.conv1(x))))
        x = self.pool(F.relu(self.bn2(self.conv2(x))))
        x = self.pool(F.relu(self.bn3(self.conv3(x))))
        x = self.pool(F.relu(self.bn4(self.conv4(x))))
        x = x.view(-1, self.num_flat_features(x))
        if self.ll_depth == 1:
            x = self.output_layer(x)
        elif self.ll_depth == 2:
            x = self.linear_layer(x)
            x = self.output_layer(x)
        elif self.ll_depth == 3:
            x = self.linear_layer(x)
            x = self.linear_layer2(x)
            x = self.output_layer(x)
        return x


    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        # print(num_features)
        # exit()
        return num_features
